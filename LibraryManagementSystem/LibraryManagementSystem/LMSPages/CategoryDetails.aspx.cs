﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem.LMSPages
{
    
    public partial class CategoryDetails : System.Web.UI.Page
    {
        string[,] SQLparams = null;
        string ErrorMsg = null;
        App_Code.DataBaseClass DB = new App_Code.DataBaseClass();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) { DB.BindGrid(Grid_Categories, "LMS", "SP_GetCategories", SQLparams, ErrorMsg); }       
        }

       
    }
}