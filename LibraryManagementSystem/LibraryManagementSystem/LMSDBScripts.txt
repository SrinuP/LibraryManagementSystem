﻿CREATE DATABASE LMS;
Create Table UserDetails(UserID int IDENTITY,UserName varchar(250),Password varchar(250),IsActive bit, CONSTRAINT PK_UserDetails PRIMARY KEY (UserID));
Create Table CollegeDetails(CollegeID int IDENTITY,CollegeName varchar(250),CollegePhoneNumber int,collegeURL varchar(500) , CONSTRAINT PK_CollegeDetails PRIMARY KEY (CollegeID));
Create Table CategoryDetails(CategoryID int IDENTITY,CategoryName varchar(250),CategoryDescription varchar(500) , CONSTRAINT PK_CategoryDetails PRIMARY KEY (CategoryID));
Create Table BooksDetails(BookID int IDENTITY,CategoryID int,BookName varchar(250),BookImageURL varchar(1000),AuthorName varchar(250),PublisherName varchar(250),Cost Decimal,OpeningCount int,ClosingCoun int, CONSTRAINT PK_BooksDetails PRIMARY KEY (BookID),CONSTRAINT FK_BookCategory FOREIGN KEY (CategoryID) References CategoryDetails(CategoryID));
Create Table StudentDetails(StudentID int IDENTITY,CollegeID int,StudentName varchar(250),PhoneNumber int,FineAmount int,NoOfBooksTaken int, CONSTRAINT PK_StudentDetails PRIMARY KEY (StudentID),CONSTRAINT FK_StudentCollege FOREIGN KEY (CollegeID) References CollegeDetails(CollegeID));
Create Table Issues(IssueID int IDENTITY,BookID int,StudentID int,IssueDate Date Default GetDate(),ExpectedReturnDate Date, CONSTRAINT PK_Issues PRIMARY KEY (IssueID),CONSTRAINT FK_IssuesBooks FOREIGN KEY (BookID) References BooksDetails(BookID),CONSTRAINT FK_IssuesStudentDetails FOREIGN KEY (StudentID) References StudentDetails(StudentID));
Create Table Returns(ReturnID int IDENTITY,IssueID int,ActualDateofReturn Date, CONSTRAINT PK_Returns PRIMARY KEY (ReturnID),CONSTRAINT FK_ReturnsIssues FOREIGN KEY (IssueID) References Issues(IssueID));

Insert into UserDetails (UserName,Password,IsActive) values ('Srinu','Hello',1);
Insert into UserDetails (UserName,Password,IsActive) values ('Navya','Hello',1);
Insert into CategoryDetails (CategoryName,CategoryDescription) values ('DBMS','DBMS');

CREATE PROCEDURE SP_GetCategories
 AS
 Select * from CategoryDetails
 GO; 