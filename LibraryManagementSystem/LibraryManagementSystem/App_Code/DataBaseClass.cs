﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace LibraryManagementSystem.App_Code
{
    
    public class DataBaseClass
    {
        enum ParamTypes
        {
            Inparam = 0,
            OutParam = 1
        }

        public string[] GetConnectionfromConfig(string ConnectionName)
        {
            string[] DBConnectionString=new string[2];
            DBConnectionString[0] = ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;
            DBConnectionString[1] = ConfigurationManager.ConnectionStrings[ConnectionName].ProviderName;
            return DBConnectionString;
        }

        public void FillParams(String[,] SQLParams, SqlCommand SQLComm)
        {
            int ParamType = 0;
            if (SQLParams != null)
            {
                if (SQLParams.Length != 0 )
                {
                    for (int I = 0; I <= (SQLParams.Length / 3 - 1); I++)
                    {
                        if (SQLParams[I, 0].Contains("OUT_"))
                        {
                            ParamType = (int)ParamTypes.OutParam;
                            break;
                        }
                    }

                    for (int I = 0; I <= (SQLParams.Length / 3 - 1); I++)
                    {
                        if (ParamType == (int)ParamTypes.Inparam)
                        {
                            SQLParams[I, 0] = SQLParams[I, 0].Replace("IN_", "@");
                            SQLComm.Parameters.AddWithValue(SQLParams[I, 0], SQLParams[I, 1]);
                        }
                        else if (ParamType == (int)ParamTypes.OutParam)
                        {
                            if (SQLParams[I, 0].Contains("IN_"))
                            {
                                SQLParams[I, 0] = SQLParams[I, 0].Replace("IN_", "@");
                                SQLComm.Parameters.Add(SQLParams[I, 0], GetSQLDBType(SQLParams[I, 2])).Direction = ParameterDirection.Input;
                                SQLComm.Parameters.Add(SQLParams[I, 0], GetSQLDBType(SQLParams[I, 2])).SqlValue = SQLParams[I, 1];
                            }
                            else if (SQLParams[I, 0].Contains("OUT_"))
                            {
                                SQLParams[I, 0] = SQLParams[I, 0].Replace("OUT_", "@");
                                SQLComm.Parameters.Add(SQLParams[I, 0], GetSQLDBType(SQLParams[I, 2])).Direction = ParameterDirection.Output;
                            }
                        }
                    }
                }
            }
        }

        public SqlDbType GetSQLDBType(String ParamDBType)
        {
            switch(ParamDBType.ToUpper())
            {
                case "INT": return SqlDbType.Int;
                case "DATE": return SqlDbType.Date;
                case "DATETIME": return SqlDbType.DateTime;
                default: return SqlDbType.VarChar;
            }
        }

        public DataTable ExecuteDataTable(string ConnectionName, String SPName, String[,] SQLParams, String ErrorMessage)
        {

            SqlConnection SQLCon = new SqlConnection(GetConnectionfromConfig(ConnectionName)[0]);
            SqlCommand SQLComm = new SqlCommand(SPName, SQLCon);
            SQLComm.CommandType = CommandType.StoredProcedure;
            FillParams(SQLParams, SQLComm);
            SqlDataAdapter SDA = new SqlDataAdapter(SQLComm);
            DataTable DT = new DataTable();
            SDA.Fill(DT);
            return DT;
        }

        public DataSet ExecuteDataset(string ConnectionName, String SPName, String[,] SQLParams, String ErrorMessage)
        {

            SqlConnection SQLCon = new SqlConnection(GetConnectionfromConfig(ConnectionName)[0]);
            SqlCommand SQLComm = new SqlCommand(SPName, SQLCon);
            SQLComm.CommandType = CommandType.StoredProcedure;
            FillParams(SQLParams, SQLComm);
            SqlDataAdapter SDA = new SqlDataAdapter(SQLComm);
            DataSet DS = new DataSet();
            SDA.Fill(DS);
            return DS;
        }

        public String ExecuteScalar(string ConnectionName, String SPName, String[,] SQLParams, String ErrorMessage)
        {
            String Out = null;
            SqlConnection SQLCon = new SqlConnection(GetConnectionfromConfig(ConnectionName)[0]);
            SqlCommand SQLComm = new SqlCommand(SPName, SQLCon);
            SQLComm.CommandType = CommandType.StoredProcedure;
            FillParams(SQLParams, SQLComm);
            try
            {
                SQLCon.Open();
                Out = SQLComm.ExecuteScalar().ToString();
            }
            catch (Exception EX)
            {
                ErrorMessage = EX.Message;
                SQLCon.Dispose();
                SQLCon.Close();
            }
            return Out;
        }

        public void ExecuteNonQuery(string ConnectionName, String SPName, String[,] SQLParams, String ErrorMessage)
        {

            SqlConnection SQLCon = new SqlConnection(GetConnectionfromConfig(ConnectionName)[0]);
            SqlCommand SQLComm = new SqlCommand(SPName, SQLCon);
            SQLComm.CommandType = CommandType.StoredProcedure;
            FillParams(SQLParams, SQLComm);
            try
            {
                SQLCon.Open();
                SQLComm.ExecuteNonQuery();
                SQLCon.Close();
            }
            catch (Exception EX)
            {
                ErrorMessage = EX.Message;
                SQLCon.Dispose();
                SQLCon.Close();
            }

        }

        public SqlDataReader ExecuteDataReader(string ConnectionName, String SPName, String[,] SQLParams, String ErrorMessage)
        {
            SqlConnection SQLCon = new SqlConnection(GetConnectionfromConfig(ConnectionName)[0]);
            SqlCommand SQLComm = new SqlCommand(SPName, SQLCon);
            SqlDataReader SQLReader = null;
            SQLComm.CommandType = CommandType.StoredProcedure;
            FillParams(SQLParams, SQLComm);
            SQLReader = SQLComm.ExecuteReader(CommandBehavior.CloseConnection);
            return SQLReader;
        }

        public void BindGrid(GridView GridID,string Connection,string SPName,string[,] SQLparams, string ErrorMsg)
        {
            try
            {
                DataTable DT = new DataTable();
                DT = ExecuteDataTable(Connection, SPName, SQLparams, ErrorMsg);
                GridID.DataSource = DT;
                GridID.DataBind();
            }
            catch (Exception ex)
            {
                ErrorMsg = ex.Message;
            }
        }
    }
}